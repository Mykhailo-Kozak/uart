/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define LEN 100

int main(void)
{
    char  filename[LEN];
    uint32 rxData;
    
    UART_Start();
    
	FILE * fileIn = fopen ("fileInputText.txt",  "r" );
	if (fileIn != NULL)
	{
		while (fgets(filename, LEN, fileIn))
		{
            UART_UartPutString(filename);
			//fprintf(stdout, "%s\n", filename);
		}
	}
	else
	{
        UART_UartPutString("file is empty or not create!\n");
		//printf("file is empty or not create!\n");
		return 1;
	}
	fclose(fileIn);
    
    
	
    CyGlobalIntEnable; /* Enable global interrupts. */
    //UART_UartPutString("Hello");
    //UART_UartPutChar(filename);
    //UART_UartGetChar();
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    FILE *fileOut=fopen("fileOutputText.txt", "a");
    for(;;)
    {
        /* Place your application code here. */
        rxData = UART_UartGetChar(); // store received characters in temporary variable
 
        if(rxData != 0) {
            UART_UartPutChar(rxData); // echo characters in terminal window
            fputc(rxData, fileOut); // Handle received characters
        }
        else
        {
            UART_UartPutString("file is empty or not create!\n");
    		//printf("file is empty or not create!\n");
    		return 1;
    	}
    }
    fclose(fileOut);
}

/* [] END OF FILE */
